using UnityEditor.SceneManagement;
using UnityEditor;
using UnityEngine;

public class LoadScenesInEditor : MonoBehaviour
{
    // Create a new drop-down menu in Editor named "Examples" and a new option called "Open Scene"
    [MenuItem("Haptics/Open Action Scene")]
    static void OpenActionScene()
    {
        //Open the Scene in the Editor (do not enter Play Mode)
        EditorSceneManager.OpenScene("Assets/Scenes/ActionsScene.unity");
        EditorSceneManager.OpenScene("Assets/Scenes/Arte.unity",OpenSceneMode.Additive);

    }
}
