using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMODUnity;

public class FloorTrigger : MonoBehaviour
{
    StudioEventEmitter footsteps;
    // Start is called before the first frame update
    void Start()
    {
        footsteps = GetComponent<StudioEventEmitter>();
        footsteps.SetParameter("Surface", 1);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            print("player concrete");
            footsteps.SetParameter("Surface", 1);
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            print("player grass");
            footsteps.SetParameter("Surface", 0);
        }
    }
    public void StepSoundEvent()
    {
        footsteps.Play();
    }
}
