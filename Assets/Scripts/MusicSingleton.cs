using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMODUnity;
using UnityEngine.SceneManagement;

public class MusicSingleton : MonoBehaviour
{
    StudioEventEmitter music;
    [HideInInspector] public bool canChangeMusic;
    // Start is called before the first frame update
    void Awake()
    {
        GameObject[] objs = GameObject.FindGameObjectsWithTag("Music");

        if (objs.Length > 1)
        {
            Destroy(this.gameObject);
        }
        DontDestroyOnLoad(this.gameObject);
        music = GetComponent<StudioEventEmitter>();
    }
    private void Start()
    {
        //Scene scene = SceneManager.GetActiveScene();
        //if(scene.name == "Menu")
        //{
        //    music.SetParameter("Music State", 0);
        //}
        //else if(scene.name == "ActionsScene")
        //{
        //    music.SetParameter("Music State", 1);
        //}
    }

    // Update is called once per frame
    void Update()
    {
        Scene scene = SceneManager.GetActiveScene();
        if (scene.name == "Menu")
        {
            music.SetParameter("Music State", 0);
        }
        else if (scene.name == "ActionsScene" && !canChangeMusic)
        {
            music.SetParameter("Music State", 1);
        }
        else if (scene.name == "Main" && !canChangeMusic)
        {
            music.SetParameter("Music State", 3);
        }
    }
}
