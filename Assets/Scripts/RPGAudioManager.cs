using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMODUnity;

public class RPGAudioManager : MonoBehaviour
{
    StudioEventEmitter raymondEmitter;
    [SerializeField] StudioEventEmitter[] drinkEmitter;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(FindMusic());
        //raymondEmitter = GameObject.Find("Music").GetComponent<StudioEventEmitter>();
    }

    IEnumerator FindMusic()
    {
        yield return new WaitForSeconds(1);
        raymondEmitter = GameObject.FindWithTag("Music").GetComponent<StudioEventEmitter>();
        if(raymondEmitter == null)
        {
            print("not found");
        }
        else
        {
            print("found");
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnRaymond(int state)
    {
        raymondEmitter.SetParameter("Raymond State", state);
    }
    public void OnDrink(int drink)
    {
        if (drink == 0)
        {
            drinkEmitter[0].Play();
        }
        else if (drink == 1)
        {
            drinkEmitter[1].Play();
            raymondEmitter.GetComponent<MusicSingleton>().canChangeMusic = true;
            raymondEmitter.SetParameter("Music State", 2);
        }
        else if (drink == 2)
        {
            drinkEmitter[2].Play();
        }
        else if (drink == 3)
        {
            drinkEmitter[3].Play();
        }
        else if (drink == 4)
        {
            drinkEmitter[4].Play();
        }
        else if (drink == 5)
        {
            drinkEmitter[5].Play();
        }
        else if (drink == 6)
        {
            drinkEmitter[6].Play();
        }
        else if (drink == 7)
        {
            drinkEmitter[7].Play();
        }
    }
}
