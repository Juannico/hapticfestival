using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AutoScrollToSelected : MonoBehaviour
{
    public ScrollRect scrollRect;
    public RectTransform content;
    public Transform selectedItem;

    public void ScrollToSelected()
    {
        if (scrollRect != null && content != null && selectedItem != null)
        {
            // Calculate the normalized position of the selected item
            Vector3 selectedItemPos = selectedItem.position;
            Vector3 contentPos = content.position;
            float normalizedY = (selectedItemPos.y - contentPos.y) / (content.rect.height - scrollRect.viewport.rect.height);

            // Ensure the normalized position is within [0, 1]
            normalizedY = Mathf.Clamp01(normalizedY);

            // Set the verticalNormalizedPosition to scroll to the selected item
            scrollRect.verticalNormalizedPosition = 1f - normalizedY;
        }
    }
}
