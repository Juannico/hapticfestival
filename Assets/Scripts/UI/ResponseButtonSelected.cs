using PixelCrushers.DialogueSystem;
using UnityEngine.EventSystems;

public class ResponseButtonSelected : StandardUIResponseButton
{
    public AutoScrollToSelected autoScroll;

    public override void Awake()
    {
        base.Awake();
        autoScroll = GetComponentInParent<AutoScrollToSelected>();
    }


    public override void OnSelect(BaseEventData eventData)
    {
        base.OnSelect(eventData);
        if (autoScroll != null)
        {
            autoScroll.selectedItem = transform;
            autoScroll.ScrollToSelected();
        }
    }
}
