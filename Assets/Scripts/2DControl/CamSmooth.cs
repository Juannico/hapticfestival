using UnityEngine;

public class SmoothFollow2D : MonoBehaviour
{
    /// <summary>
    /// Looks for the gameobject with tag "Player" on the scene so that it can be assigned
    /// </summary>
    private Transform target; // El objeto que la c�mara seguir�
    public float smoothTime = 0.2f; // Tiempo para alcanzar la posici�n deseada
    public Vector3 offset;

    private Vector3 velocity = Vector3.zero;

    private void Awake()
    {
        if (target == null)
        {
            target = GameObject.FindGameObjectWithTag("Player").transform;
        }
    }
    private void FixedUpdate()
    {
        // Calcula la posici�n deseada de la c�mara
        Vector3 targetPosition = target.position + offset;

       
        if (target.position != targetPosition)
        {
            transform.position = Vector3.SmoothDamp(transform.position, targetPosition, ref velocity, smoothTime);
        }
        else // Si el jugador se detiene centra la camara de manera suave
        {
            transform.position = Vector3.SmoothDamp(transform.position, targetPosition, ref velocity, smoothTime * 2f);
        }
    }
}
