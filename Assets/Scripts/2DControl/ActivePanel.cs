using UnityEngine;

public class PanelController : MonoBehaviour
{
    public GameObject panel; 

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.N)) 
        {
            if (panel != null)
            {
                panel.SetActive(false); // Desactivar el panel
            }
        }
    }
}
