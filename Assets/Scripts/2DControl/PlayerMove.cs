using PixelCrushers.DialogueSystem;
using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class PlayerMove2d : MonoBehaviour
{
    public enum IdleDirection
    {
        Front = 0,
        Back = 1,
        Left = 2,
        Right = 3,
    }
    [SerializeField] private float speedMove = 5f;
    [SerializeField] private float speedMultiplier = 2f; // Multiplier for running speed

    private Rigidbody2D playerRb2d;
    private Vector2 moveInput;
    private Animator pAnimator;
    private bool isRunning = false; // Flag to indicate if the player is running
    private bool canRun = false;
    private IdleDirection idleDirection;

    FloorTrigger floor;


    void Start()
    {
        playerRb2d = GetComponent<Rigidbody2D>();
        pAnimator = GetComponent<Animator>();

        DialogueManager.AddLuaObserver("Variable['CanRun']", LuaWatchFrequency.EndOfConversation, CheckCanRunCondition);
        StartCoroutine(FindFloor());
    }
    IEnumerator FindFloor()
    {
        yield return new WaitForSeconds(0.5f);
        floor = GameObject.FindWithTag("FloorSteps").GetComponent<FloorTrigger>();
    }
    void Update()
    {
        float moveX = Input.GetAxisRaw("Horizontal");
        float moveY = Input.GetAxisRaw("Vertical");
        moveInput = new Vector2(moveX, moveY).normalized;
        SetIdleDirection();
        pAnimator.SetFloat("Horizontal", moveX);
        pAnimator.SetFloat("Vertical", moveY);
        pAnimator.SetFloat("Speed", moveInput.sqrMagnitude);

        // Check if the "B" key is pressed and set the isRunning flag accordingly
        if (Input.GetKey(KeyCode.Z))
        {
            isRunning = true;
        }
        else
        {
            isRunning = false;
        }
    }

    private void FixedUpdate()
    {
        // Move the player based on the moveInput and currentSpeed
        playerRb2d.MovePosition(playerRb2d.position + moveInput * GetCurrentSpeed() * Time.fixedDeltaTime);
    }

    private void CheckCanRunCondition(LuaWatchItem item, Lua.Result result)
    {
        canRun = result.asBool;
    }

    private float GetCurrentSpeed()
    {
        return isRunning && canRun ? speedMove * speedMultiplier : speedMove;
    }

    private void SetIdleDirection()
    {
        if (moveInput.magnitude == 0) return;

        if (moveInput.x > 0)
        {
            idleDirection = IdleDirection.Right;
        }
        else if (moveInput.y > 0)
        {
            idleDirection = IdleDirection.Back;
        }
        else if (moveInput.x < 0)
        {
            idleDirection = IdleDirection.Left;
        }
        else
        {
            idleDirection = IdleDirection.Front;
        }
        pAnimator.SetFloat("IdleDirection", (float)idleDirection / 3);
    }
    public void StepEvent()
    {
        floor.StepSoundEvent();
    }
}
