using UnityEngine;
using UnityEngine.Tilemaps;

public class MusicalTile : MonoBehaviour
{
    public Color newColor;
    public Tilemap tilemap;

    private Vector3Int previousTilePos;

    private void Start()
    {
        previousTilePos = tilemap.WorldToCell(transform.position);
    }

    private void Update()
    {
        Vector3Int currentTilePos = tilemap.WorldToCell(transform.position);

        if (currentTilePos != previousTilePos)
        {
            TileBase tile = tilemap.GetTile(previousTilePos);
            if (tile != null)
            {
                tilemap.SetTileFlags(previousTilePos, TileFlags.None);
                tilemap.SetColor(previousTilePos, Color.white);
            }

            tile = tilemap.GetTile(currentTilePos);
            if (tile != null)
            {
                tilemap.SetTileFlags(currentTilePos, TileFlags.None);
                tilemap.SetColor(currentTilePos, newColor);
            }

            previousTilePos = currentTilePos;
        }
    }
}
