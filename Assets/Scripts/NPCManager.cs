using PixelCrushers.DialogueSystem;
using System.Collections;
using System.Collections.Generic;
using Tamarillo;
using UnityEngine;

public class NPCManager : MonoBehaviour
{
    public BoxCollider2D wall;
    public SceneManager sceneManager;
    public GameObject guadalupeNPC;

    private bool canRunConditionMet = false;
    private bool vipPassConditionMet = false;

    private void Start()
    {
        wall = GetComponent<BoxCollider2D>();

        DialogueManager.AddLuaObserver("Variable['CanRun']", LuaWatchFrequency.EndOfConversation, CheckCanRunCondition);
        DialogueManager.AddLuaObserver("Variable['VipPass']", LuaWatchFrequency.EndOfConversation, CheckVipPassCondition);
        DialogueManager.AddLuaObserver("Variable['StartRythm']", LuaWatchFrequency.EndOfConversation, StartRythmGame);
    }

    private void CheckCanRunCondition(LuaWatchItem item, Lua.Result result)
    {
        canRunConditionMet = result.asBool;
        CheckConditions();
    }

    private void CheckVipPassCondition(LuaWatchItem item, Lua.Result result)
    {
        vipPassConditionMet = result.asBool;
        CheckConditions();
    }

    private void CheckConditions()
    {
        if (canRunConditionMet && vipPassConditionMet)
        {
            wall.gameObject.SetActive(false);
        }
    }

    private void StartRythmGame(LuaWatchItem item, Lua.Result result)
    {
        if (result.asBool == true)
        {
            sceneManager.LoadRythmGame();
            DialogueLua.SetVariable("StartRythm", false);
        }
    }
}
