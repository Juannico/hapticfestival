using NaughtyAttributes;
using System.Collections;
using System.Collections.Generic;
using TamarilloTools;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Tamarillo
{
    public class SceneManager : MonoBehaviour
    {
        [Scene] public int assetScene;
        [Scene] public int rythGameScene;

        // List of Banks to load
        [FMODUnity.BankRef]
        public List<string> Banks = new List<string>();

        // The name of the scene to load and switch to
        public string Scene = null;
        private void Start()
        {
            Scene scene = UnityEngine.SceneManagement.SceneManager.GetActiveScene();
            if (!UnityEngine.SceneManagement.SceneManager.GetSceneByBuildIndex(assetScene).isLoaded && scene.name != "PreMenu")
                UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(assetScene,LoadSceneMode.Additive);

            if (scene.name == "PreMenu")
                StartCoroutine(LoadGameAsync());
        }

        void Update()
        {
            // Update the loading indication
        }

        IEnumerator LoadGameAsync()
        {
            // Start an asynchronous operation to load the scene
            AsyncOperation async = UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(assetScene);

            // Don't let the scene start until all Studio Banks have finished loading
            async.allowSceneActivation = false;

            // Iterate all the Studio Banks and start them loading in the background
            // including the audio sample data
            foreach (var bank in Banks)
            {
                FMODUnity.RuntimeManager.LoadBank(bank, true);
            }

            // Keep yielding the co-routine until all the bank loading is done
            // (for platforms with asynchronous bank loading)
            while (!FMODUnity.RuntimeManager.HaveAllBanksLoaded)
            {
                yield return null;
            }

            // Keep yielding the co-routine until all the sample data loading is done
            while (FMODUnity.RuntimeManager.AnySampleDataLoading())
            {
                yield return null;
            }

            // Allow the scene to be activated. This means that any OnActivated() or Start()
            // methods will be guaranteed that all FMOD Studio loading will be completed and
            // there will be no delay in starting events
            async.allowSceneActivation = true;

            // Keep yielding the co-routine until scene loading and activation is done.
            while (!async.isDone)
            {
                yield return null;
            }

        }

        public void LoadRythmGame()
        {
            StartCoroutine(WaitForSound());
        }
        IEnumerator WaitForSound()
        {
            yield return new WaitForSeconds(1);
            UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(rythGameScene);
        }
    }
}

