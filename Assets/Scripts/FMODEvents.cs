using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMODUnity;

public class FMODEvents : MonoBehaviour
{
    [field: Header("Music")]
    [field: SerializeField] public EventReference music { get; set; }
    public static FMODEvents instance;
    // Start is called before the first frame update
    void Awake()
    {
        if (instance != null)
        {
            Debug.LogError("duplicated audio manager");
        }
        instance = this;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
