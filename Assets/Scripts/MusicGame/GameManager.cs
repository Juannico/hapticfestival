﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using FMODUnity;

public class GameManager : MonoBehaviour
{
    [SerializeField] string parameterName, labelName;
    [SerializeField] float parameterValue;
    [SerializeField] StudioEventEmitter /*gameEmitter,*/ missEmitter, strikeEmitter;
    StudioEventEmitter gameEmitter;
    public GameObject hitEffect, goodEffect, perfectEffect, missEffect;

    //public AudioSource sourceMusic;
    public bool startPlaying;
    public BeatScroller bs;
    public static GameManager instance;
    public int currentScore;
    public int scorePerNote;
    public int scorePerGood = 125;
    public int scorePerPerfect = 150;

    public float totalNotes;
    public float normalHits;
    public float goodHits;
    public float perfectHits;
    public float missHits;

    public int currentMulti = 1;
    public int multiTrack;
    public int[] multiThresholds;

    public Text scoreText;
    public Text multiText;
    public Text time;
    public Text beatText;
    public Text dialogueText;

    public AudioClip rocksong;
    public GameObject startpanel;
    public GameObject resultsCreen;
    public Text percentageText, normalText, goodText, perfectText, missText, rankText, finalScoreText;
    public GameObject rockSet;

    public GameObject panelcancion, finalBtn, inicioBtn;

    //vfx
    GameObject[] rayos;
    float vfxDuration;

    int goodHitCount;

    public float segundosTemporalesDeCorte;
    float beat;
    bool startCountingTime, playStrikeOnce;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(StartBtn());
        inicioBtn.SetActive(false);
        finalBtn.SetActive(false);
        gameEmitter = GameObject.Find("Music").GetComponent<StudioEventEmitter>();
        gameEmitter.GetComponent<MusicSingleton>().canChangeMusic = false;
        dialogueText.text = "Presiona los botones con las teclas A, S, D y F cuando las notas toquen el botón de su color.";
        goodHitCount = 0;
        rayos = GameObject.FindGameObjectsWithTag("Lightning");
        foreach (GameObject rayo in rayos)
        {
            rayo.SetActive(false);
        }
        instance = this;
        scoreText.text = "Score: " + 0;
        currentMulti = 1;

        totalNotes = FindObjectsOfType<NoteObject>().Length;
        rockSet.SetActive(false);

        panelcancion.SetActive(false);
    }
    IEnumerator StartBtn()
    {
        yield return new WaitForSeconds(1.5f);
        inicioBtn.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if(!finalBtn.activeSelf && inicioBtn.activeSelf)
                ChooseSong("rock");
            else if (finalBtn.activeSelf)
                FinalButton();
        }
        if (startCountingTime)
        {
            segundosTemporalesDeCorte -= Time.deltaTime;
            beat += 1.8795f * Time.deltaTime;
            beatText.text = "Beat: " + beat.ToString("0");
            if((int)beat % 4 == 0)
            {
                beatText.color = Color.green;
            }
            else
            {
                beatText.color = Color.white;
            }
            time.text = "Tiempo: " + segundosTemporalesDeCorte.ToString("0");
            if(segundosTemporalesDeCorte < 0)
            {
                segundosTemporalesDeCorte = 0;
                EndGame();
            }
        }
        if (startPlaying)
        {
            if (/*!sourceMusic.isPlaying && */!resultsCreen.activeInHierarchy)
            {
                resultsCreen.SetActive(true);

                normalText.text = normalHits.ToString();
                goodText.text = goodHits.ToString();
                perfectText.text = perfectHits.ToString();
                missText.text = missHits.ToString();

                float percentageHit = (normalHits + goodHits + perfectHits) / totalNotes * 100;
                percentageText.text = percentageHit.ToString("F2") + "%";

                string rankVal = "F";

                if (percentageHit > 40)
                {
                    rankVal = "D";
                    if (percentageHit > 55)
                    {
                        rankVal = "C";
                        if (percentageHit > 70)
                        {
                            rankVal = "B";
                            if (percentageHit > 85)
                            {
                                rankVal = "A";
                                if (percentageHit > 95)
                                {
                                    rankVal = "S";
                                }
                            }
                        }
                    }
                }
                rankText.text = rankVal;
                finalScoreText.text = currentScore.ToString();
            }
        }
        if (goodHitCount >= 30 && goodHitCount <= 70)
        {
            vfxDuration += Time.deltaTime;
            if (!playStrikeOnce)
            {
                gameEmitter.SetParameter("Minigame 1 Streak", 1);
                strikeEmitter.Play();
                playStrikeOnce = true;
            }
            foreach (GameObject rayo in rayos)
            {
                rayo.SetActive(true);
            }
        }
        else if (goodHitCount >= 70 || vfxDuration >= 5f)
        {
            playStrikeOnce = false;
            gameEmitter.SetParameter("Minigame 1 Streak", 0);
            strikeEmitter.Stop();
            goodHitCount = 0;
            vfxDuration = 0f;
        }
        else
        {
            foreach (GameObject rayo in rayos)
            {
                rayo.SetActive(false);
            }
        }
    }

    public void NoteHit()
    {
        //goodHitCount++;
        if (currentMulti-1<multiThresholds.Length)
        {
            multiTrack++;

            if (multiThresholds[currentMulti - 1] <= multiTrack)
            {
                multiTrack = 0;
                currentMulti++;
            }
        }
        multiText.text = "Multiplier: x" + currentMulti;
        scoreText.text = "Score: "+currentScore;
    }

    public void NormalHit()
    {
        goodHitCount++;
        hitEffect.SetActive(true);
        currentScore += scorePerNote * currentMulti;
        NoteHit();
        normalHits++;
    }

    public void GoodHit()
    {
        goodHitCount++;
        goodEffect.SetActive(true);
        currentScore += scorePerGood * currentMulti;
        NoteHit();
        goodHits++;
    }

    public void PerfectHit()
    {
        goodHitCount++;
        perfectEffect.SetActive(true);
        currentScore += scorePerPerfect * currentMulti;
        NoteHit();
        perfectHits++;
    }

    public void NoteMiss()
    {
        missEmitter.Play();
        goodHitCount = 0;
        missEffect.SetActive(true);
        currentMulti = 1;
        multiTrack = 0;
        multiText.text = "Multiplier: x" + currentMulti;
        missHits++;
    }



    public void ChooseSong (string option)
    {
        switch (option)
        {
            case "rock":
                rockSet.SetActive(true);
                //AudioManager.instance.ChangeMusic(parameterName, parameterValue);
                //AudioManager.instance.ChangeMusicLabel(parameterName, labelName);
                //emitter.Stop();
                gameEmitter.GetComponent<MusicSingleton>().canChangeMusic = true;
                gameEmitter.SetParameter("Music State", 4);
                //emitter.SetParameter("Music State", 4);
                startCountingTime = true;
                //gameEmitter.Play();
                //AudioManager.instance.ChangeMusic(changeMusic);
                //sourceMusic.clip = rocksong;

                break;

        }
        bs.hasStarted = true;
        startpanel.SetActive(false);
        startPlaying = true;
        //sourceMusic.Play();
        //AudioManager.instance
        //panelcancion.SetActive(true);

    }

    public void Reset()
    {
        StartCoroutine(Restart());
    }

    public IEnumerator Restart ()
    {
        yield return new WaitForSeconds(1);
        //Scene scene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(1);
    }

    public void FinalButton()
    {
        dialogueText.text = "ELDERBROOK: ¡Estupendo! Tienes mucho talento, Rebeca. Ahora es mi turno, así que siéntete libre de seguir explorando el festival.";
        Reset();
    }

    void EndGame()
    {
        startpanel.SetActive(true);
        finalBtn.SetActive(true);
        inicioBtn.SetActive(false);
        dialogueText.text = "Tu puntaje fue " + currentScore.ToString() + ". ¡Fantástico! Ahora puedes seguir disfrutando del festival.";
        bs.hasStarted = false;
    }
}
