﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FXObject : MonoBehaviour
{
    public float lifeTime = 1;

    private void OnEnable()
    {
        StartCoroutine(Deactivate(lifeTime));
        gameObject.GetComponentInChildren<ParticleSystem>().Play();
    }

    IEnumerator Deactivate(float time)
    {
        yield return new WaitForSeconds(time);
        gameObject.SetActive(false);
    }
}
