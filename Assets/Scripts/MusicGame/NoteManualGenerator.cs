using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoteManualGenerator : MonoBehaviour
{
    public GameObject[] notes;
    public GameObject NoteParent, tope;
    public int id;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            id = 0;
            GameObject clone = Instantiate(notes[id], new Vector3(notes[id].transform.position.x, tope.transform.position.y, notes[id].transform.position.z), Quaternion.identity);
            clone.transform.SetParent(NoteParent.transform);
            clone.GetComponent<NoteObject>().ResetNote();
        }
        else if (Input.GetKeyDown(KeyCode.S))
        {
            id = 1;
            GameObject clone = Instantiate(notes[id], new Vector3(notes[id].transform.position.x, tope.transform.position.y, notes[id].transform.position.z), Quaternion.identity);
            clone.transform.SetParent(NoteParent.transform);
            clone.GetComponent<NoteObject>().ResetNote();
        }
        else if (Input.GetKeyDown(KeyCode.D))
        {
            id = 2;
            GameObject clone = Instantiate(notes[id], new Vector3(notes[id].transform.position.x, tope.transform.position.y, notes[id].transform.position.z), Quaternion.identity);
            clone.transform.SetParent(NoteParent.transform);
            clone.GetComponent<NoteObject>().ResetNote();
        }
        else if (Input.GetKeyDown(KeyCode.F))
        {
            id = 3;
            GameObject clone = Instantiate(notes[id], new Vector3(notes[id].transform.position.x, tope.transform.position.y, notes[id].transform.position.z), Quaternion.identity);
            clone.transform.SetParent(NoteParent.transform);
            clone.GetComponent<NoteObject>().ResetNote();
        }
    }
}
