﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoteObject : MonoBehaviour
{
    public bool canBePressed;
    public bool gotRegistered;
    public bool traste;
    ParticleSystem particles;
    public int id;
    private void Start()
    {
        if (!traste)
        {
            particles = gameObject.transform.GetChild(0).GetComponent<ParticleSystem>();
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Activator"))
        {
            canBePressed = true;
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.CompareTag("Activator") && collision.GetComponent<ButtonController>().id == id)
        {
            if (collision.GetComponent<ButtonController>().gotPressed)
            {
                if (canBePressed)
                {
                    gotRegistered = true;
                    Color tmp = GetComponent<SpriteRenderer>().color;
                    tmp.a = 0f;
                    GetComponent<SpriteRenderer>().color = tmp;
                    //StartCoroutine(Disappear());

                    if (Mathf.Abs(transform.position.y) > 0.25f)
                    {
                        particles.Play();
                        GameManager.instance.NormalHit();
                    }
                    else if (Mathf.Abs(transform.position.y) > 0.1f)
                    {
                        particles.Play();
                        GameManager.instance.GoodHit();
                    }
                    else
                    {
                        particles.Play();
                        GameManager.instance.PerfectHit();
                    }
                }
            }
        }
    }

    IEnumerator Disappear ()
    {
        yield return new WaitForSeconds(1);
        gameObject.SetActive(false);
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Activator"))
        {
            canBePressed = false;
            if (!gotRegistered)
            {
                GameManager.instance.NoteMiss();
            }
        }
    }
    public void ResetNote()
    {
        StartCoroutine(ResetCoroutine());
    }
    IEnumerator ResetCoroutine()
    {
        yield return new WaitForSeconds(1.5f);
        gotRegistered = false;
        canBePressed = false;
        Color tmp = GetComponent<SpriteRenderer>().color;
        tmp.a = 1f;
        GetComponent<SpriteRenderer>().color = tmp;
    }

}
