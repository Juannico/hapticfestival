﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ButtonController : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    private SpriteRenderer spriteRen;
    public Sprite defaultImg;
    public Sprite pressedImg;
    public bool gotPressed;
    public int id;
    // Start is called before the first frame update
    void Start()
    {
        spriteRen = GetComponent<SpriteRenderer>();
    }
    //tengo que hacer que cada botón se conecte con las notas de su color y no con las otras notas, hay 4 colores
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.A) && id == 0)
        {
             spriteRen.sprite = pressedImg;
             gotPressed = true;
        }
        else if (Input.GetKeyDown(KeyCode.S) && id == 1)
        {
            spriteRen.sprite = pressedImg;
            gotPressed = true;
        }
        else if (Input.GetKeyDown(KeyCode.D) && id == 2)
        {
            spriteRen.sprite = pressedImg;
            gotPressed = true;
        }
        else if (Input.GetKeyDown(KeyCode.F) && id == 3)
        {
            spriteRen.sprite = pressedImg;
            gotPressed = true;
        }

        if (Input.GetKeyUp(KeyCode.A) && id == 0)
        {
            spriteRen.sprite = defaultImg;
            gotPressed = false;
        }
        else if (Input.GetKeyUp(KeyCode.S) && id == 1)
        {
            spriteRen.sprite = defaultImg;
            gotPressed = false;
        }
        else if (Input.GetKeyUp(KeyCode.D) && id == 2)
        {
            spriteRen.sprite = defaultImg;
            gotPressed = false;
        }
        else if (Input.GetKeyUp(KeyCode.F) && id == 3)
        {
            spriteRen.sprite = defaultImg;
            gotPressed = false;
        }
    }
    public void OnPointerDown(PointerEventData eventData)
    {
        spriteRen.sprite = pressedImg;
        gotPressed = true;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        spriteRen.sprite = defaultImg;
        gotPressed = false;
    }
}
